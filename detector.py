import cv2
import argparse
import numpy as np
import os
import multiprocessing as mp
from time import time
import glob
import pandas as pd
# handle command line arguments
ap = argparse.ArgumentParser()
ap.add_argument('-s', '--source', required=True,
                help = 'path to images')
ap.add_argument('-d', '--dest', required=True,
                help = 'destinaton of images')
ap.add_argument('-c', '--config', required=True,
                help = 'path to yolo config file')
ap.add_argument('-w', '--weights', required=True,
                help = 'path to yolo pre-trained weights')
ap.add_argument('-cl', '--classes', required=True,
                help = 'path to text file containing class names')
args = ap.parse_args()
if not os.path.exists(args.dest):
    os.makedirs(args.dest)
# function to get the output layer names
# in the architecture
def get_output_layers(net):
    layer_names = net.getLayerNames()

    output_layers = [layer_names[i[0] - 1] for i in net.getUnconnectedOutLayers()]

    return output_layers
# function to draw bounding box on the detected object with class name
def draw_bounding_box(img, class_id, confidence, x, y, x_plus_w, y_plus_h, class_names):
    # read class names from text file
    with open(class_names, 'r') as f:
        classes = [line.strip() for line in f.readlines()]
    # generate different colors for different classes
    COLORS = [[0, 255, 255], [255, 0, 127]]
    label = str(classes[class_id])

    color = COLORS[class_id]

    cv2.rectangle(img, (x, y), (x_plus_w, y_plus_h), color, 5)

    cv2.putText(img, label+" "+str(round(confidence*100,2))+"%", (x - 10, y - 10), cv2.FONT_HERSHEY_SIMPLEX,
                img.shape[0]/(img.shape[0]-img.shape[0]/1.5), color, 10)
# Funtion to get filenames in a path
def get_filenames(base):
    X0 = []
    ext = ('jpg', 'jpeg', 'png', 'tif', 'PNG', 'JPG', 'JPEG')
    for name in glob.glob(base+"/*"):
        if name.endswith(ext):
            X0.append(name)
    return X0
# Function to detect objets single image
def detect(imag,weights,config,class_names):
    # read input image
    image = cv2.imread(imag)

    Width = image.shape[1]
    Height = image.shape[0]
    scale = 0.00392

    # read pre-trained model and config file
    net = cv2.dnn.readNet(weights, config)

    # create input blob
    blob = cv2.dnn.blobFromImage(image, scale, (416, 416), (0, 0, 0), True, crop=False)

    # set input blob for the network
    net.setInput(blob)

    # run inference through the network
    # and gather predictions from output layers
    outs = net.forward(get_output_layers(net))

    # initialization
    class_ids = []
    confidences = []
    boxes = []
    conf_threshold = 0.5
    nms_threshold = 0.4

    # for each detetion from each output layer
    # get the confidence, class id, bounding box params
    # and ignore weak detections (confidence < 0.5)
    for out in outs:
        for detection in out:
            scores = detection[5:]
            class_id = np.argmax(scores)
            confidence = scores[class_id]
            if confidence > 0.5:
                center_x = int(detection[0] * Width)
                center_y = int(detection[1] * Height)
                w = int(detection[2] * Width)
                h = int(detection[3] * Height)
                x = center_x - w / 2
                y = center_y - h / 2
                class_ids.append(class_id)
                confidences.append(float(confidence))
                boxes.append([x, y, w, h])
    # apply non-max suppression
    indices = cv2.dnn.NMSBoxes(boxes, confidences, conf_threshold, nms_threshold)
    # go through the detections remaining
    # after nms and draw bounding box
    macula_c = []
    macula_p = []
    nervio_c = []
    nervio_p = []
    for i in indices:
        i = i[0]
        box = boxes[i]
        x = box[0]
        y = box[1]
        w = box[2]
        h = box[3]

        draw_bounding_box(image, class_ids[i], confidences[i], round(x), round(y), round(x + w), round(y + h), class_names)
        if class_ids[i] == 0:
            macula_c.append("OK")
            macula_p.append(round(confidences[i],4))
        elif class_ids[i] == 1:
            nervio_c.append("OK")
            nervio_p.append(round(confidences[i],4))

    return macula_c, nervio_c, macula_p, nervio_p, image
# Function to detect objets
def detect_1(imag):
    # read input image
    image = cv2.imread(imag)

    Width = image.shape[1]
    Height = image.shape[0]
    scale = 0.00392
    class_names = args.classes
    # read pre-trained model and config file
    net = cv2.dnn.readNet(args.weights, args.config)

    # create input blob
    blob = cv2.dnn.blobFromImage(image, scale, (416, 416), (0, 0, 0), True, crop=False)

    # set input blob for the network
    net.setInput(blob)

    # run inference through the network
    # and gather predictions from output layers
    outs = net.forward(get_output_layers(net))

    # initialization
    class_ids = []
    confidences = []
    boxes = []
    conf_threshold = 0.5
    nms_threshold = 0.4

    # for each detetion from each output layer
    # get the confidence, class id, bounding box params
    # and ignore weak detections (confidence < 0.5)
    for out in outs:
        for detection in out:
            scores = detection[5:]
            class_id = np.argmax(scores)
            confidence = scores[class_id]
            if confidence > 0.5:
                center_x = int(detection[0] * Width)
                center_y = int(detection[1] * Height)
                w = int(detection[2] * Width)
                h = int(detection[3] * Height)
                x = center_x - w / 2
                y = center_y - h / 2
                class_ids.append(class_id)
                confidences.append(float(confidence))
                boxes.append([x, y, w, h])
    # apply non-max suppression
    indices = cv2.dnn.NMSBoxes(boxes, confidences, conf_threshold, nms_threshold)
    # go through the detections remaining
    # after nms and draw bounding box
    macula_c = []
    macula_p = []
    nervio_c = []
    nervio_p = []
    for i in indices:
        i = i[0]
        box = boxes[i]
        x = box[0]
        y = box[1]
        w = box[2]
        h = box[3]
        draw_bounding_box(image, class_ids[i], confidences[i], round(x), round(y), round(x + w), round(y + h), class_names)
        if class_ids[i] == 0:
            macula_c.append("OK")
            macula_p.append(round(confidences[i],4))
        elif class_ids[i] == 1:
            nervio_c.append("OK")
            nervio_p.append(round(confidences[i],4))
    file, ext = os.path.splitext(imag)
    filename = os.path.split(file)
    # save output image to disk
    cv2.imwrite(os.path.join(args.dest,filename[1])+"_detection" + ext, image)
    return macula_c, nervio_c, macula_p, nervio_p

def main():
    pool = mp.Pool(processes=mp.cpu_count())
    X0= get_filenames(args.source)
    start = time()
    M_c, N_c, M_p, N_p = zip(*pool.map(detect_1, X0))
    stop = time()
    pool.close()
    pool.join()
    filenames = []
    n = 0
    for i in X0:
        file = os.path.split(i)
        filenames.append(file[1])
        n += 1

    df1 = pd.DataFrame({'File': filenames,
                        'Class_macula': M_c,
                        'Class_nerve': N_c,
                        'Proba_macula': M_p,
                        'Proba_nerve':N_p
                        })
    df1.to_csv(args.source+'_results.csv')
    et = stop - start
    print('Total images: ' + str(n))
    print('[Elapsed time]: ' + str(et) + ' s')

if __name__ == '__main__':
    main()